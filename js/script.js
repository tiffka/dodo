﻿document.querySelectorAll('.input-block .input').forEach(function (el) {
    el.onfocus = el.onclick = function () {
        this.parentNode.classList.add("active");
    };
    el.onblur = function () {
        if (!this.value) this.parentNode.classList.remove("active");
    };
});
